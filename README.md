# Computer graphics lab

Programs from [Computer graphics course at MIMUW](https://usosweb.mimuw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1000-135GK&lang=en)
rewritten in [Rust](https://www.rust-lang.org/) using [Glium](https://github.com/glium/glium) and [Nalgebra](https://nalgebra.org/). 
Based on [prof. Przemysław Kiciak's](https://www.mimuw.edu.pl/~przemek/index_en.html) [source code](https://www.mimuw.edu.pl/~przemek/przemek1_files/glsl-progII.tar.gz).