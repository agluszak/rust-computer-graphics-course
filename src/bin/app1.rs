use glium::glutin;
use glium::glutin::event::VirtualKeyCode;
use glium::Surface;
use glium::{implement_vertex, uniform};

use glm::U4;
use nalgebra_glm as glm;

use std::f32::consts::PI;

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 3],
    color: [f32; 3],
}
implement_vertex!(Vertex, position, color);

fn icosahedron() -> ([Vertex; 12], [u16; 62]) {
    const A: f32 = 0.52573115;
    const B: f32 = 0.85065085;
    let vertices = [
        Vertex {
            position: [-A, 0.0, -B],
            color: [1.0, 0.0, 0.0],
        },
        Vertex {
            position: [A, 0.0, -B],
            color: [1.0, 0.5, 0.0],
        },
        Vertex {
            position: [0.0, -B, -A],
            color: [1.0, 1.0, 0.0],
        },
        Vertex {
            position: [-B, -A, 0.0],
            color: [0.5, 1.0, 0.0],
        },
        Vertex {
            position: [-B, A, 0.0],
            color: [0.0, 1.0, 0.0],
        },
        Vertex {
            position: [0.0, B, -A],
            color: [0.0, 1.0, 0.5],
        },
        Vertex {
            position: [A, 0.0, B],
            color: [0.0, 1.0, 1.0],
        },
        Vertex {
            position: [-A, 0.0, B],
            color: [0.0, 0.5, 1.0],
        },
        Vertex {
            position: [0.0, -B, A],
            color: [0.0, 0.0, 1.0],
        },
        Vertex {
            position: [B, -A, 0.0],
            color: [0.5, 0.0, 1.0],
        },
        Vertex {
            position: [B, A, 0.0],
            color: [1.0, 0.0, 1.0],
        },
        Vertex {
            position: [0.0, B, A],
            color: [1.0, 0.0, 0.5],
        },
    ];
    let indices = [
        0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3, /* lamana, od 0 */
        7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7, 1, 10, 6, /* lamana, od 25 */
        2, 3, 4, 5, 8, 9, 10, 11, /* 4 odcinki, od 28 */
        0, 1, 2, 3, 4, 5, 1, /* wachlarz, od 36 */
        6, 7, 8, 9, 10, 11, 7, /* wachlarz, od 43  */
        1, 9, 2, 8, 3, 7, 4, 11, 5, 10, 1, 9u16, /* tasma, od 50 */
    ];
    (vertices, indices)
}

enum State {
    POINTS,
    EDGES,
    FACES,
}

fn main() {
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new().with_depth_buffer(24);
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    let vertex_shader_src = include_str!("../app1.glsl.vert");
    let fragment_shader_src = include_str!("../app1.glsl.frag");

    let program =
        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None)
            .unwrap();

    let (vertices, indices) = icosahedron();

    let shape = glium::vertex::VertexBuffer::new(&display, &vertices).unwrap();

    let point_indices = glium::index::NoIndices(glium::index::PrimitiveType::Points);

    let edge_indices_1 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LineStrip,
        &indices[0..25],
    )
    .unwrap();
    let edge_indices_2 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LineStrip,
        &indices[25..28],
    )
    .unwrap();
    let edge_indices_3 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LinesList,
        &indices[28..36],
    )
    .unwrap();

    let face_indices_1 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleFan,
        &indices[36..43],
    )
    .unwrap();
    let face_indices_2 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleFan,
        &indices[43..50],
    )
    .unwrap();
    let face_indices_3 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleStrip,
        &indices[50..62],
    )
    .unwrap();

    let mut state = State::FACES;

    event_loop.run(move |event, _, control_flow| {
        if let glutin::event::Event::WindowEvent {
            event: window_event,
            ..
        } = event
        {
            match window_event {
                glutin::event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit;
                    return;
                }
                glutin::event::WindowEvent::KeyboardInput {
                    input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::K),
                            ..
                        },
                    ..
                } => state = State::EDGES,
                glutin::event::WindowEvent::KeyboardInput {
                    input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::W),
                            ..
                        },
                    ..
                } => state = State::POINTS,
                glutin::event::WindowEvent::KeyboardInput {
                    input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::S),
                            ..
                        },
                    ..
                } => state = State::FACES,
                _ => (),
            }
        }

        let next_frame_time =
            std::time::Instant::now() + std::time::Duration::from_nanos(16_666_667);
        *control_flow = glutin::event_loop::ControlFlow::WaitUntil(next_frame_time);

        // drawing
        let mut target = display.draw();
        target.clear_color_and_depth((1.0, 1.0, 1.0, 1.0), 1.0);

        let (width, height) = target.get_dimensions();
        let aspect = width as f32 / height as f32;

        let perspective_matrix = glm::perspective(aspect, PI / 12.0, 5.0, 15.0);

        let eye = glm::vec3(0.0, 0.0, 10.0);
        let center = glm::vec3(0.0, 0.0, 1.0);
        let up = glm::vec3(0.0, 1.0, 0.0);
        let view_matrix = glm::look_at_rh::<f32>(&eye, &center, &up);

        let model_matrix = glm::identity::<f32, U4>();

        let uniforms = uniform! {
            mm: *model_matrix.as_ref(),
            vm: *view_matrix.as_ref(),
            pm: *perspective_matrix.as_ref()
        };

        let depth = glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLess,
            write: true,
            ..Default::default()
        };

        let point_size = match state {
            State::POINTS => Some(5.0),
            _ => None,
        };

        let draw_params = glium::DrawParameters {
            depth,
            point_size,
            ..Default::default()
        };

        match state {
            State::POINTS => {
                target
                    .draw(&shape, &point_indices, &program, &uniforms, &draw_params)
                    .unwrap();
            }
            State::EDGES => {
                target
                    .draw(&shape, &edge_indices_1, &program, &uniforms, &draw_params)
                    .unwrap();
                target
                    .draw(&shape, &edge_indices_2, &program, &uniforms, &draw_params)
                    .unwrap();
                target
                    .draw(&shape, &edge_indices_3, &program, &uniforms, &draw_params)
                    .unwrap();
            }
            State::FACES => {
                target
                    .draw(&shape, &face_indices_1, &program, &uniforms, &draw_params)
                    .unwrap();
                target
                    .draw(&shape, &face_indices_2, &program, &uniforms, &draw_params)
                    .unwrap();
                target
                    .draw(&shape, &face_indices_3, &program, &uniforms, &draw_params)
                    .unwrap();
            }
        }

        target.finish().unwrap();
    });
}
