use glium::glutin;
use glium::glutin::event::{VirtualKeyCode, MouseButton};
use glium::Surface;
use glium::{implement_vertex, uniform};

use glm::U4;
use nalgebra_glm as glm;

use std::f32::consts::PI;
use glium::backend::glutin::glutin::dpi::PhysicalPosition;
use glium::backend::glutin::glutin::event::ElementState;
use nalgebra_glm::{Vec3, U3, TVec3};

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 3],
    color: [f32; 3],
}
implement_vertex!(Vertex, position, color);

fn icosahedron() -> ([Vertex; 12], [u16; 62]) {
    const A: f32 = 0.52573115;
    const B: f32 = 0.85065085;
    let vertices = [
        Vertex {
            position: [-A, 0.0, -B],
            color: [1.0, 0.0, 0.0],
        },
        Vertex {
            position: [A, 0.0, -B],
            color: [1.0, 0.5, 0.0],
        },
        Vertex {
            position: [0.0, -B, -A],
            color: [1.0, 1.0, 0.0],
        },
        Vertex {
            position: [-B, -A, 0.0],
            color: [0.5, 1.0, 0.0],
        },
        Vertex {
            position: [-B, A, 0.0],
            color: [0.0, 1.0, 0.0],
        },
        Vertex {
            position: [0.0, B, -A],
            color: [0.0, 1.0, 0.5],
        },
        Vertex {
            position: [A, 0.0, B],
            color: [0.0, 1.0, 1.0],
        },
        Vertex {
            position: [-A, 0.0, B],
            color: [0.0, 0.5, 1.0],
        },
        Vertex {
            position: [0.0, -B, A],
            color: [0.0, 0.0, 1.0],
        },
        Vertex {
            position: [B, -A, 0.0],
            color: [0.5, 0.0, 1.0],
        },
        Vertex {
            position: [B, A, 0.0],
            color: [1.0, 0.0, 1.0],
        },
        Vertex {
            position: [0.0, B, A],
            color: [1.0, 0.0, 0.5],
        },
    ];
    let indices = [
        0, 1, 2, 0, 3, 4, 0, 5, 1, 9, 2, 8, 3, // a line strip, from 0
        7, 4, 11, 5, 10, 9, 6, 8, 7, 6, 11, 7, 1, 10, 6, // a line strip, from 25
        2, 3, 4, 5, 8, 9, 10, 11, // 4 lines, from 28
        0, 1, 2, 3, 4, 5, 1, // a triangle fan, from 36
        6, 7, 8, 9, 10, 11, 7, // a triangle fan, from 43
        1, 9, 2, 8, 3, 7, 4, 11, 5, 10, 1, 9u16, // a triangle strip, from 50
    ];
    (vertices, indices)
}

enum DrawingState {
    POINTS,
    EDGES,
    FACES,
}

fn main() {
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new().with_depth_buffer(24);
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    let vertex_shader_src = include_str!("../app1.glsl.vert");
    let fragment_shader_src = include_str!("../app1.glsl.frag");

    let program =
        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None)
            .unwrap();

    let (vertices, indices) = icosahedron();

    let shape = glium::vertex::VertexBuffer::new(&display, &vertices).unwrap();

    let point_indices = glium::index::NoIndices(glium::index::PrimitiveType::Points);

    let edge_indices_1 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LineStrip,
        &indices[0..25],
    )
        .unwrap();
    let edge_indices_2 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LineStrip,
        &indices[25..28],
    )
        .unwrap();
    let edge_indices_3 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::LinesList,
        &indices[28..36],
    )
        .unwrap();

    let face_indices_1 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleFan,
        &indices[36..43],
    )
        .unwrap();
    let face_indices_2 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleFan,
        &indices[43..50],
    )
        .unwrap();
    let face_indices_3 = glium::index::IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TriangleStrip,
        &indices[50..62],
    )
        .unwrap();

    let mut drawing_state = DrawingState::FACES;
    let mut dragging = false;
    let mut mouse_x: f32 = 0.0;
    let mut mouse_y: f32 = 0.0;
    let mut model_matrix = glm::identity::<f32, U4>();
    let mut now = std::time::Instant::now();

    let mut eye = glm::vec3(0.0, 0.0, -10.0);
    let mut rotation_axis = glm::vec3(1.0, 0.0, 0.0);
    let mut rotation_angle = 0.0;

    let center = glm::vec3(0.0, 0.0, 1.0);
    let up = glm::vec3(0.0, 1.0, 0.0);
    let mut view_matrix = glm::look_at_rh::<f32>(&eye, &center, &up);

    let mut mouse_delta_x: f32 = 0.0;
    let mut mouse_delta_y: f32 = 0.0;
    event_loop.run(move |event, _, control_flow| {
        match event {
            glutin::event::Event::WindowEvent {
                event: window_event,
                ..
            } =>
                match window_event {
                    glutin::event::WindowEvent::CloseRequested => {
                        *control_flow = glutin::event_loop::ControlFlow::Exit;
                        return;
                    }
                    glutin::event::WindowEvent::KeyboardInput {
                        input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::K),
                            ..
                        },
                        ..
                    } => drawing_state = DrawingState::EDGES,
                    glutin::event::WindowEvent::KeyboardInput {
                        input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::W),
                            ..
                        },
                        ..
                    } => drawing_state = DrawingState::POINTS,
                    glutin::event::WindowEvent::KeyboardInput {
                        input:
                        glutin::event::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::S),
                            ..
                        },
                        ..
                    } => drawing_state = DrawingState::FACES,
                    glutin::event::WindowEvent::CursorMoved {
                        position: PhysicalPosition { x, y }, ..
                    } => {
                        let x = x as f32;
                        let y = y as f32;
                        mouse_delta_x = x - mouse_x;
                        mouse_delta_y = y - mouse_y;
                        mouse_x = x;
                        mouse_y = y;
                    }
                    glutin::event::WindowEvent::MouseInput { state: ElementState::Pressed, button: MouseButton::Left, .. } => dragging = true,
                    glutin::event::WindowEvent::MouseInput { state: ElementState::Released, button: MouseButton::Left, .. } => dragging = false,
                    _ => (),
                }

            glutin::event::Event::MainEventsCleared => {
                let mut target = display.draw();
                target.clear_color_and_depth((1.0, 1.0, 1.0, 1.0), 1.0);

                let (width, height) = target.get_dimensions();
                let aspect = width as f32 / height as f32;

                let perspective_matrix = glm::perspective(aspect, PI / 12.0, 1.0, 100.0);

                if dragging && (mouse_delta_x != 0.0 || mouse_delta_y != 0.0) {
                    let mut drag_vec = glm::vec3(mouse_delta_y / height as f32, mouse_delta_x / width as f32, 0.0);
                    drag_vec = glm::normalize::<f32, U3>(&drag_vec);
                    const DRAG_AMOUNT: f32 = -PI / 60.0;
                    let (new_axis, new_angle) = rotate(&rotation_axis, &rotation_angle, &drag_vec, &DRAG_AMOUNT);
                    rotation_axis = new_axis;
                    rotation_angle = new_angle;
                    view_matrix = glm::identity::<f32, U4>();
                    view_matrix = glm::translate(&view_matrix, &eye);
                    view_matrix = glm::rotate(&view_matrix, -rotation_angle, &rotation_axis);

                    mouse_delta_x = 0.0;
                    mouse_delta_y = 0.0
                }


                let uniforms = uniform! {
                    mm: *model_matrix.as_ref(),
                    vm: *view_matrix.as_ref(),
                    pm: *perspective_matrix.as_ref()
                };

                let depth = glium::Depth {
                    test: glium::draw_parameters::DepthTest::IfLess,
                    write: true,
                    ..Default::default()
                };

                let point_size = match drawing_state {
                    DrawingState::POINTS => Some(5.0),
                    _ => None,
                };

                let draw_params = glium::DrawParameters {
                    depth,
                    point_size,
                    ..Default::default()
                };

                match drawing_state {
                    DrawingState::POINTS => {
                        target
                            .draw(&shape, &point_indices, &program, &uniforms, &draw_params)
                            .unwrap();
                    }
                    DrawingState::EDGES => {
                        target
                            .draw(&shape, &edge_indices_1, &program, &uniforms, &draw_params)
                            .unwrap();
                        target
                            .draw(&shape, &edge_indices_2, &program, &uniforms, &draw_params)
                            .unwrap();
                        target
                            .draw(&shape, &edge_indices_3, &program, &uniforms, &draw_params)
                            .unwrap();
                    }
                    DrawingState::FACES => {
                        target
                            .draw(&shape, &face_indices_1, &program, &uniforms, &draw_params)
                            .unwrap();
                        target
                            .draw(&shape, &face_indices_2, &program, &uniforms, &draw_params)
                            .unwrap();
                        target
                            .draw(&shape, &face_indices_3, &program, &uniforms, &draw_params)
                            .unwrap();
                    }
                }

                target.finish().unwrap();
            }
            _ => ()
        }
    });
}

fn rotate(v2: &TVec3<f32>, phi2: &f32, v1: &TVec3<f32>, phi1: &f32) -> (TVec3<f32>, f32) {
    let s1 = (0.5 * phi1).sin();
    let s2 = (0.5 * phi2).sin();
    let c1 = (0.5 * phi1).cos();
    let c2 = (0.5 * phi2).cos();
    let v2v1 = glm::dot(v2, v1);
    let v2xv1 = glm::cross(v2, v1);
    let c = c2 * c1 - v2v1 * s2 * s1;

    let mut v = glm::vec3(0.0, 0.0, 0.0);
    v.x = v2.x * s2 * c1 + v1.x * s1 * c2 + &v2xv1.x * s2 * s1;
    v.y = v2.y * s2 * c1 + v1.y * s1 * c2 + &v2xv1.y * s2 * s1;
    v.z = v2.z * s2 * c1 + v1.z * s1 * c2 + &v2xv1.z * s2 * s1;

    let s = (glm::dot(&v, &v)).sqrt();

    if s > 0.0 {
        v.x /= s;
        v.y /= s;
        v.z /= s;
        let phi = 2.0 * s.atan2(c);
        (v, phi)
    } else {
        v.x = 1.0;
        v.y = 0.0;
        v.z = 0.0;
        let phi = 0.0;
        (v, phi)
    }
}