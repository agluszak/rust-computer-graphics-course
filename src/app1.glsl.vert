#version 420 

in vec3 position;
in vec3 color;

out vec3 vert_color;

uniform  mat4 mm;
uniform  mat4 vm;
uniform  mat4 pm;

void main() {
    vec4 position_4 = vec4(position, 1.0);
    gl_Position = pm * (vm * (mm * position_4));
    vert_color = color;
}
